# Altemoi
#Developer: CASTRO Delfor Hernán

#ES: Proyecto de desarrollo de aplicación web para la administración informativa
de pequeñas y medianas estaciones de ómnibus. 'Altemoi significa «ómnibus» en
lengua toba.


#EN: Web application development project for the informative administration of
small and medium bus stations. 'Altemoi means «bus» on qom language.


Diseño y desarrollo de aplicación web para la gestión informativa de arribos y
partidas de buses.


Desarrollado por CASTRO, Delfor Hernán

Para el trayecto de Desarrollo de la Tecnicatura Universitaria en Software Libre,
de la Universidad Nacional del Litoral. Santa Fe. Argentina.

Directora: BASCONCEL, Brisa

Codirector: MASTAGLIA, Nicolás

Resumen:

Se propone el diseño y desarrollo de una aplicación web que permita la gestión
informativa de arribos y partidas de buses en estaciones de colectivos. La
aplicación estará dirigida a terminales de tamaño pequeño a mediano. La
aplicación deberá permitir dos tipos de funcionamiento: (a) un modo “controlador”,
en el cual un usuario registra los eventos en la terminal (se entenderá por evento
el arribo o la partida de un bus); y (b) un modo “automático”, en el cual el
sistema supondrá salidas y arribos de forma predeterminada en función de una tabla
de horarios previstos. El sistema podrá ponerse en modo “controlador” cuando haya
un usuario autorizado para registrar eventos, y en modo “automático” en aquellos
casos en los que la terminal de ómnibus quede sin personal dedicado a esa tarea.
El sistema debe ser capaz de realizar las tareas CRUD (creación, lectura,
actualización y eliminación) de las entidades definidas en el modelo en la etapa
de análisis (por ejemplo: boleterías, empresas de colectivos), cargar los
horarios previstos de arribo y partida de coches (los cuales serán usados para
el modo “automático”) y registrar los eventos. También el sistema debe permitir
acceder a una interfaz web pública en la cual se muestre una pantalla informativa
con los últimos arribos y partidas, además de soportar el ingreso de información
extra, como por ejemplo alertas o advertencias de seguridad.

Desde el aspecto técnico, está previsto que el desarrollo se lleve a cabo usando
Django, Bootstrap como framework para HTML,
y un lenguaje de programación asincrónico del lado del cliente (probablemente
AJAX). Se prevé usar como motor de base de datos MySQL o MariaDB.



Se prevé además la creación de la documentación de uso de la aplicación resultante.


#Análisis de la necesidad.

Dada mis actividades cotidianas, soy un ciudadano que frecuentemente transito por
estaciones de ómnibus, dado que dependo de ese servicio para trasladarme a otras
ciudades. En mi caso, resido en la ciudad de Victoria, una pequeña ciudad que
cuenta con una terminal de ómnibus obsoleta y poco accesible. Las terminales de
ómnibus que transito más frecuentemente son las de Victoria, Rosario y Paraná.
Con menor frecuencia, soy usuario de las estaciones de Nogoyá, Santa Fe y Retiro.

En estas estaciones he observado lo problemático que resulta para el usuario
poder tener notificaciones respecto del arribo y partida de buses aunque, claro
está, en cada una de ellas tiene implicancias diferentes dada las dimensiones
físicas de las mismas.

De aquí en adelante denominaré “alertas” a todos los eventos que serán inherentes
al sistema, tales como partidas y arribos de ómnibus.



Así, por ejemplo, en la Terminal de Ómnibus de Rosario, se ha implementado un
sistema de altoparlantes, que notifica a partir de una voz pregrabada, cuáles
servicios se encuentran arribando, y cuáles partiendo. El sistema de alertas
carece de pantallas. Ésta solución tecnológica presenta varios déficits: la voz
por parlantes muchas veces no alcanza a oírse claramente; además, no se reitera,
por lo que si el usuario no pudo oír el aviso ya no tendrá la posibilidad de
volver a escucharlo. Por otra parte, al no contar con pantallas, las personas
hipoacúsicas no tienen acceso a tal información.



En la Terminal de Ómnibus de Paraná existe un sistema de altoparlantes en los que
una operadora humana informa los eventos. Aunque el sonido es más claro que en la
Terminal de Ómnibus de Rosario, también carece de un sistema de pantallas. En
algunos horarios, en particular a la noche, no siempre hay operadora humana para
hablar por altoparlantes, quedando la terminal sin ese servicio de alertas.



En los casos de las Terminales de Ómnibus de Victoria y de Nogoyá, ambas ciudades
pequeñas del interior, no existen mecanismos de alertas. En el caso de la Terminal
de Victoria, es a veces el chofer de la unidad o el maletero (un hombre en
situación de calle que vive en la estación) quien informa, usando el método del
grito (chofer.gritar(destino) JAJA ), el destino hacia donde se dirige. Sin dudas,
esto se reitera en numerosas terminales de ómnibus de las pequeñas ciudades del
país.



El trabajo de investigación pretende proponer una aproximación a una solución
tecnológica para estaciones de pequeña y mediana escala. Se da por hecho que las
estaciones de gran escala, por lo generales de ciudades capitales, cuentan con
algún tipo de mecanismo de alertas, aunque a veces con algunos déficits, como en
el caso de la Terminal de Rosario. El foco principal estará en generar un
sistema que permita dotar a las terminales de un mecanismo basado en alertas
por pantalla, que permita a los usuarios seguir los eventos. Implementar pantallas
permite, además, hacer llegar a los usuarios otros tipos de informaciones en un
futuro, tales como informaciones sanitarias, información de emergencias,
turística, de gobierno, etcétera, aunque ello escapa al interés de esta etapa
del desarrollo.



#Estado del arte



Las soluciones que existen para implementar sistemas de alertas son escasas, y
privativas. A modo de ejemplo, puedo citar la tesis “Sistema informativo a
clientes en Estaciones ASTRO”, disponible en http://www.ilustrados.com/documentos/sistema-informativo-clientes-estaciones-astro-050208.pdf,
donde se aborda el análisis de un sistema para estaciones de ómnibus. El proyecto,
de origen cubano, y elaborado como tesis de final de carrera, lamentablemente no
está disponible (al menos a partir de la búsqueda que hice) como sotware libre,
y buena parte de las herramientas que se han empleado para su desarrollo han sido
privativas, aunque los autores reconocen hacia el final del trabajo a modo de
desafío migrar hacia herramientas libres. Dicho trabajo, aunque no emplea
herramientas tecnológicas acordes a estos tiempos, ni tampoco se propone con un
entorno web que permita acceder de manera más transparente desde diferentes
dispositivos y entornos operativos, tiene la virtud de hacer un análisis del
modelo de negocio bastante acabado y claro, y en el cual me he inspirado para
algunas etapas del desarrollo y requerimientos del sistema.


#Tecnologías empleadas

2021-07-15: ACTUALIZACIÓN IMPORTANTE
Si bien el proyecto original planeaba usar PHP, se definió migrar a Django.

El desarrollo se abordará usando Django. Se seguirá el patrón de diseño Model-
View-Template.
Se emplea Bootstrap como framework HTML para el manejo de los estilos.

#Objetivo general

	Producir un artefacto software funcional que permita la gestión básica de
 los aspectos informativos de una terminal de ómnibus de pequeño y mediano tamaño.

#Objetivos específicos del desarrollo, metodología y calendarización

	1.Definir las herramientas tecnológicas a emplear en base a la búsqueda y análisis de las características

	Metodología: Se realizará una comparación de las herramientas vistas durante
el cursado de las asignaturas y se agregarán otras que puedan encontrarse a partir
de la búsqueda en la web. Las herramientas tecnológicas que deben definirse son:
lenguajes de programación a usar, frameworks, sistemas de gestión de bases de datos.
Obviamente, todas estas herramientas deben ser software libre.
	Tiempo estimado: 1 mes (de junio a julio, durante el cursado de la asignatura Trabajo Final)



  2.a Desarrollar la lógica de negocios y modelar los datos necesarios para representar los problemas vinculados con la administración de arribos, partidas y datos de urgencia en una Terminal de Ómnibus


	Metodología: A partir de la observación en terminales de ómnibus y de la lectura
 del material definido en el apartado «Estado del Arte», construir las clases y métodos
básicos del dominio del problema. Así mismo se definirán las tablas y campos principales
para comenzar con las primeras funcionalidades. En este sentido, se pretende desarrollar
en el siguiente orden de subsistemas, teniendo en cuenta que luego se irá iterando y
haciendo mejoras: (a) logueo, (b)CRUD de  usuarios (IMPORTANTE: en esta etapa aún no se
trabajará con permisos de usuario), (c) CRUD de empresas de transporte, (d) CRUD de boleterías,
(e) CRUD de plataformas. Luego de esto se abordará el funcionamiento del sistema en
 modo «operador»; para ello: (f) CRU de eventos (arribos y partidas); (g) primeras pruebas de
internacionalización a inglés y portugués (no se pretende una internacionalización terminada,
pero al menos una función beta). Pruebas del funcionamiento y búsqueda de posibles
fallos en la entrada de información.
	Tiempo estimado: puntos a) al e) 1 mes (agosto); la primera quincena de
septiembre se usará para hacer revisiones e iteraciones antes de continuar.
Puntos f) y g), dos semanas (segunda quincena de septiembre).


	2.b Desarrollar las interfaces de usuario para la gestión de las entidades  definidas en los modelos.

	Metodología: Los objetivos 2.a y 2.b, dada sus naturalezas, irán fuertemente
de la mano. Se desarrollarán interfaces empleando las herramientas tecnológicas
seleccionadas. Se deberá tener como principios importantes que las mismas sean claras,
funcionales, adaptables a diferentes tipos de pantalla y accesibles. Se probarán
las mismas en al menos dos dispositivos: una computadora laptop y un teléfono móvil
con sistema operativo Android.
	Tiempo estimado: dos meses, al mismo tiempo en que se avanza con el objetivo 2.a

	3.Desarrollar un front-end de pantalla informativa para usuarios de terminal de ómnibus

	Metodología: Se desarrollará una página que sirva para mostrar los últimos
eventos de arribos y partidas. El desarrollo deberá ser con algún lenguaje asincrónico,
de modo que no recargue el servidor y los resultados sean más óptimos. La página
informativa deberá ser adaptable a diferentes tipos de pantalla, aunque su máxima
funcionalidad deberá estar enfocada a grandes pantallas. Pruebas del funcionamiento
en diferentes pantallas.
	Tiempo estimado: 1 mes (octubre)

	NOTA: Para fines de octubre, se debería contar con un prototipo altamente
funcional, pero aún en beta, del sistema final, que permita el funcionamiento del
mismo en modo operador


4.Desarrollar la lógica de negocios para permitir un funcionamiento en modo «automático» y revisar el modo «operador»

	Metodología: Se desarrollará la lógica necesaria para permitir que el sistema
de eventos funcione de manera automática ante la ausencia de un operador, anunciando
arribos y partidas previstas. Esta etapa no implica el desarrollo de nuevas
interfaces, por lo que todo el trabajo de esta etapa estará centrado en la escritura
del código necesario para escribir los algoritmos que resuelven el problema.
Pruebas del funcionamiento y búsqueda de posibles fallos.
	Tiempo estimado: Dos semanas (primera quincena de noviembre)

	5.Elaborar manual de uso para operadores de la herramienta

	Metodología: la documentación estará centrada en el uso del mismo. No se
prevé en esta etapa el desarrollo de una documentación para el desarrollador.
La información orientada al desarrollador será parte del Informe Final, pero no
es una etapa del desarrollo propiamente dicha.
	Tiempo estimado: Dos semanas (segunda quincena de noviembre)

	6.Permitir la internacionalización al inglés y portugués (funcionalidad que quede al menos en fase beta)

	Metodología: avanzar con la internacionalización del proyecto a partir
de las herramientas que brinden las tecnologías seleccionadas.
	Tiempo estimado: en proceso continuo.

	NOTA: A fin de noviembre el desarrollo debería estar completado con las
funcionalidades previstas para ésta etapa. Durante el mes de diciembre y enero
se avanzará con la elaboración del Informe Final, que incluirá información técnica,
operativa, y el código de la aplicación.
