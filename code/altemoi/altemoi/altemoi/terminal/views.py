import json
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.core import serializers #https://docs.djangoproject.com/en/4.2/topics/serialization/
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from datetime import datetime

# Vistas de la aplicación 'terminal'
from terminal.models import SysParameters, Lvidaqa, Lasom, Lmala, Nmen, Status, Alhua, Alhuana, Yinoyic, Iqueuoxon, Inoxoneuo, InoxoneuoIntermedio, Deyachi, DeyachiIntermedio, NmenIqueuoxon
from terminal.forms import ParametroForm, LvidaqaForm, LasomForm, LmalaForm, NmenForm, StatusForm, AlhuaForm, AlhuanaForm, YinoyicForm, IqueuoxonFom, InoxoneuoForm, InoxoneuoIntermedioForm, DeyachiForm, DeyachiIntermedioForm, NmenIqueuoxonForm

#Vistas para CRUD Terminal


def Tablon(request):
	terminales = Lvidaqa.objects.all()
	puertas = Lasom.objects.all()
	plataformas = Lmala.objects.all()
	boleterias = Nmen.objects.all()

	contexto = {'terminales': terminales,'puertas': puertas, 'plataformas':plataformas, 'boleterias': boleterias}

	return render(request, 'tablon/tablon.html', contexto)


#Vistas para CRUD parámetros
class NuevoParametro (CreateView):
	model = SysParameters
	form_class = ParametroForm
	template_name = 'iot/parametro.html'
	success_url = reverse_lazy('terminal:lista_parametro')

class ListaParametro (ListView):
	model = SysParameters
	template_name = 'nachaxan/parametro.html'

class EditarParametro (UpdateView):
	model = SysParameters
	form_class = ParametroForm
	template_name = 'iot/parametro.html'
	success_url = reverse_lazy('terminal:lista_parametro')

class EliminarParametro (DeleteView):
	model = SysParameters
	template_name = 'aqtac/parametro.html'
	success_url = reverse_lazy('terminal:lista_parametro')


class NuevaTerminal (CreateView):
	model = Lvidaqa
	form_class = LvidaqaForm
	template_name = 'iot/lvidaqa.html'
	success_url = reverse_lazy('terminal:lista_lvidaqa')

class ListaTerminal (ListView):
	model = Lvidaqa
	template_name = 'nachaxan/lvidaqa.html'

class EditarTerminal (UpdateView):
	model = Lvidaqa
	form_class = LvidaqaForm
	template_name = 'iot/lvidaqa.html'
	success_url = reverse_lazy('terminal:lista_lvidaqa')

class EliminarTerminal (DeleteView):
	model = Lvidaqa
	template_name = 'aqtac/lvidaqa.html'
	success_url = reverse_lazy('terminal:tablon')



#Vistas para CRUD Puerta
class NuevaPuerta (CreateView):
	model = Lasom
	form_class = LasomForm
	template_name = 'iot/lasom.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaPuerta (ListView):
	model = Lasom
	template_name = 'nachaxan/lasom.html'

class EditarPuerta (UpdateView):
	model = Lasom
	form_class = LasomForm
	template_name = 'iot/lasom.html'
	success_url = reverse_lazy('terminal:tablon')

class EliminarPuerta (DeleteView):
	model = Lasom
	template_name = 'aqtac/lasom.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD Plataforma
class NuevaPlataforma (CreateView):
	model = Lmala
	form_class = LmalaForm
	template_name = 'iot/lmala.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaPlataforma (ListView):
	model = Lmala
	template_name = 'nachaxan/lmala.html'


class EditarPlataforma (UpdateView):
	model = Lmala
	form_class = LmalaForm
	template_name = 'iot/lmala.html'
	success_url = reverse_lazy('terminal:tablon')

class EliminarPlataforma (DeleteView):
	model = Lmala
	template_name = 'aqtac/lmala.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD Boleteria
class NuevaBoleteria (CreateView):
	model = Nmen
	form_class = NmenForm
	template_name = 'iot/nmen.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaBoleteria (ListView):
	model = Nmen
	template_name = 'nachaxan/nmen.html'

class EditarBoleteria(UpdateView):
	model = Nmen
	form_class = NmenForm
	template_name = 'iot/nmen.html'
	success_url = reverse_lazy('terminal:tablon')

class ElimininarBoleteria (DeleteView):
	model = Nmen
	template_name = 'aqtac/nmen.html'
	success_url = reverse_lazy('terminal:tablon')


#Vistas para CRUD Status
class NuevoStatus (CreateView):
	model = Status
	form_class = StatusForm
	template_name = 'iot/status.html'
	success_url = reverse_lazy('terminal:lista_status')

class ListaStatus (ListView):
	model = Status
	template_name = 'nachaxan/status.html'

class EditarStatus (UpdateView):
	model = Status
	form_class = StatusForm
	template_name = 'iot/status.html'
	success_url = reverse_lazy('terminal:lista_status')

class EliminarStatus (DeleteView):
	model = Status
	template_name = 'aqtac/status.html'
	success_url = reverse_lazy('terminal:lista_status')

#Vistas para CRUD países
class NuevoPais (CreateView):
	model = Alhua
	form_class = AlhuaForm
	template_name = 'iot/alhua.html'
	success_url = reverse_lazy('terminal:lista_alhua')

class ListaPais (ListView):
	model = Alhua
	template_name = 'nachaxan/alhua.html'

class EditarPais (UpdateView):
	model = Alhua
	form_class = AlhuaForm
	template_name = 'iot/alhua.html'
	success_url = reverse_lazy('terminal:lista_alhua')

class EliminarPais (DeleteView):
	model = Alhua
	template_name = 'aqtac/alhua.html'
	success_url = reverse_lazy('terminal:lista_alhua')

#Vistas para CRUD provincias
class NuevaProvincia (CreateView):
	model = Alhuana
	form_class = AlhuanaForm
	template_name = 'iot/alhuana.html'
	success_url = reverse_lazy('terminal:lista_alhuana')

class ListaProvincia (ListView):
	model = Alhuana
	template_name = 'nachaxan/alhuana.html'

class EditarProvincia (UpdateView):
	model = Alhuana
	form_class = AlhuanaForm
	template_name = 'iot/alhuana.html'
	success_url = reverse_lazy('terminal:lista_alhuana')

class EliminarProvincia (DeleteView):
	model = Alhuana
	template_name = 'aqtac/alhuana.html'
	success_url = reverse_lazy('terminal:lista_alhuana')

#Vistas para CRUD ciudades
class NuevaCiudad (CreateView):
	model = Yinoyic
	form_class = YinoyicForm
	template_name = 'iot/yinoyic.html'
	success_url = reverse_lazy('terminal:lista_yinoyic')

class ListaCiudad (ListView):
	model = Yinoyic
	template_name = 'nachaxan/yinoyic.html'

class EditarCiudad (UpdateView):
	model = Yinoyic
	form_class = YinoyicForm
	template_name = 'iot/yinoyic.html'
	success_url = reverse_lazy('terminal:lista_yinoyic')

class EliminarCiudad (DeleteView):
	model = Yinoyic
	template_name = 'aqtac/yinoyic.html'
	success_url = reverse_lazy('terminal:lista_yinoyic')

#Vistas para CRUD Empresas de transporte
class NuevaEmpresa (CreateView):
	model = Iqueuoxon
	form_class = IqueuoxonFom
	template_name = 'iot/iqueuoxon.html'
	success_url = reverse_lazy('terminal:tablon')

class ListaEmpresa (ListView):
	model = Iqueuoxon
	template_name = 'nachaxan/iqueuoxon.html'

class VerEmpresa (DetailView):
	model = Iqueuoxon

	def get_context_data(self, *args, **kwargs):
		context = super(VerEmpresa, self).get_context_data(*args, **kwargs)

		#Acá añado al contexto las boleterías que tienen esta empresa asociada
		context['boleterias'] = NmenIqueuoxon.objects.filter(empresa=self.object)
		return context

	template_name = 'nachaxan/iqueuoxonDetails.html'


class EditarEmpresa (UpdateView):
	model = Iqueuoxon
	form_class = IqueuoxonFom
	template_name = 'iot/iqueuoxon.html'
	success_url = reverse_lazy('terminal:tablon')

class EliminarEmpresa (DeleteView):
	model = Iqueuoxon
	template_name = 'aqtac/iqueuoxon.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD para ingresos
class NuevoIngreso (CreateView):
	model = Inoxoneuo
	form_class = InoxoneuoForm
	template_name = 'iot/inoxoneuo.html'
	success_url = reverse_lazy('terminal:lista_inoxoneuo')

class ListaIngreso (ListView):
	model = Inoxoneuo

	def get_context_data(self, *args, **kwargs):
		context = super(ListaIngreso, self).get_context_data(*args, **kwargs)
		context['ingresos'] = Inoxoneuo.objects.all()
		return context

	template_name = 'nachaxan/inoxoneuo.html'


#def ListaIngreso(request):
#	ingresos = Inoxoneuo.objects.all()[0:5]
#	context = {'ingresos':ingresos, }

class EditarIngreso (UpdateView):
	model = Inoxoneuo
	form_class = InoxoneuoForm
	template_name = 'iot/inoxoneuo.html'
	success_url = reverse_lazy('terminal:lista_inoxoneuo')

class EliminarIngreso (DeleteView):
	model = Inoxoneuo
	template_name = 'aqtac/inoxoneuo.html'
	success_url = reverse_lazy('terminal:tablon')

#Vistas para CRUD ingresos ciudades intermedias
def DetalleIngreso (request, id_ingreso):
	#recupero el ingreso padre, del que se mostrarán los ingresos intermedios
	ingreso = Inoxoneuo.objects.get(id=id_ingreso)

	#recupero los intermedios existentes para ese ingreso
	intermedios = InoxoneuoIntermedio.objects.filter(inoxoneuo=id_ingreso)


	#genero el context para pasar al template
	context = {'ingreso':ingreso, 'intermedios':intermedios}

	#retorno el template y le mando el contexto
	return render (request, 'nachaxan/inoxoneuointermedio.html', context)

def NuevoIngresoIntermedio (request, id_ingreso):

	#genero el contexto
	ing = id_ingreso
	ingreso = Inoxoneuo.objects.get(id=id_ingreso)
	ciudadesAsociadas = InoxoneuoIntermedio.objects.values_list('ciudad').filter(inoxoneuo=ing)

	#y luego con un exclude obtengo las ciudades no asociadas al recorrido
	ciudadesNoAsociadas = Yinoyic.objects.exclude(id__in=ciudadesAsociadas)

	context = {'ingreso' : ingreso,
				'asociadas' : ciudadesAsociadas,
				'noAsociadas' : ciudadesNoAsociadas}

	form = InoxoneuoIntermedioForm (request.POST or None)

	if request.method == 'POST':
		if form.is_valid():
			form.save()
		return redirect ('terminal:detalle_inoxoneuo', id_ingreso = ing)

	context['form'] = form

	return render (request, "iot/inoxoneuointermedio.html", context)

class EliminarIngresoIntermedio(DeleteView):
	model = InoxoneuoIntermedio
	template_name = 'aqtac/inoxoneuointermedio.html'

	def get_success_url(self):
		ingreso = self.object.inoxoneuo
		#mando como argumento el parámetro id_ingreso

		return reverse_lazy('terminal:detalle_inoxoneuo', kwargs={'id_ingreso': ingreso.pk})

#Vistas para CRUD para egresos
class NuevoEgreso (CreateView):
	model = Deyachi
	form_class = DeyachiForm
	template_name = 'iot/deyachi.html'
	success_url = reverse_lazy('terminal:lista_deyachi')

class ListaEgreso (ListView):
	model = Deyachi
	template_name = 'nachaxan/deyachi.html'
	#resolví traer información de childs (destinos intermedios)
	#en el stackoverflow https://stackoverflow.com/questions/49072454/django-generic-listview-two-models

class EditarEgreso (UpdateView):
	model = Deyachi
	form_class = DeyachiForm
	template_name = 'iot/deyachi.html'
	success_url = reverse_lazy('terminal:lista_deyachi')

class EliminarEgreso (DeleteView):
	model = Deyachi
	template_name = 'aqtac/deyachi.html'
	success_url = reverse_lazy('terminal:lista_deyachi')

#Vistas para CRUD ingresos ciudades intermedias
def DetalleEgreso (request, id_egreso):
	#recupero el ingreso padre, del que se mostrarán los ingresos intermedios
	egreso = Deyachi.objects.get(id=id_egreso)

	#recupero los intermedios existentes para ese ingreso
	intermedios = DeyachiIntermedio.objects.filter(deyachi=id_egreso)


	#genero el context para pasar al template
	context = {'egreso':egreso, 'intermedios':intermedios}

	#retorno el template y le mando el contexto
	return render (request, 'nachaxan/deyachiintermedio.html', context)

def NuevoEgresoIntermedio (request, id_egreso):

	#genero el contexto
	egr = id_egreso
	egreso = Deyachi.objects.get(id=id_egreso)
	ciudadesAsociadas = DeyachiIntermedio.objects.values_list('ciudad').filter(deyachi=egr)

	#y luego con un exclude obtengo las ciudades no asociadas al recorrido
	ciudadesNoAsociadas = Yinoyic.objects.exclude(id__in=ciudadesAsociadas)

	context = {'egreso' : egreso,
				'asociadas' : ciudadesAsociadas,
				'noAsociadas' : ciudadesNoAsociadas}

	form = DeyachiIntermedioForm (request.POST or None)

	if request.method == 'POST':
		if form.is_valid():
			form.save()
		return redirect ('terminal:detalle_deyachi', id_egreso = egr)

	context['form'] = form

	return render (request, "iot/deyachiintermedio.html", context)


class EliminarEgresoIntermedio(DeleteView):
	model = DeyachiIntermedio
	template_name = 'aqtac/deyachiintermedio.html'

	def get_success_url(self):
		egreso = self.object.deyachi
		#mando como argumento el parámetro id_ingreso

		return reverse_lazy('terminal:detalle_deyachi', kwargs={'id_egreso': egreso.pk})

def VerMonitor(request):
	#creo instancia de datetime
	fecha = datetime.now()
	#identifico fecha  actual (retorna integer 0 al 6)
	x = fecha.weekday()

	#identifico la hora actual
	h = fecha.strftime("%H:%M:%S")
	#acá hago un diccionario para después "convertir" ese número
	#correspondiente al día, en la letra adecuada para la query
	dias = {"0":"l",
	"1":"m", "2":"x", "3":"j", "4":"v", "5":"s", "6":"d"}

	y = dias[str(x)]

	columna_query = y
	search_type = 'exact'
	filtro = columna_query + '__' + search_type
	#info=members.filter(**{ filter: search_string })


	ingresos_rows = SysParameters.objects.filter(parameter="filas_ingresos").first()
	ingresos = Inoxoneuo.objects.select_related("empresa").filter(**{ filtro: 1}).filter(horaArribo__gte= h)[:int(ingresos_rows.value)]
	context = {'ingresos': ingresos, 'y': y, 'h':h}


	return render (request, 'nachaxan/monitor.html', context)

def Monitor(request):


	#creo instancia de datetime
	fecha = datetime.now()
	#identifico fecha  actual (retorna integer 0 al 6)
	x = fecha.weekday()

	#identifico la hora actual
	h = fecha.strftime("%H:%M:%S")
	#acá hago un diccionario para después "convertir" ese número
	#correspondiente al día, en la letra adecuada para la query
	dias = {"0":"l",
	"1":"m", "2":"x", "3":"j", "4":"v", "5":"s", "6":"d"}

	y = dias[str(x)]

	columna_query = y
	search_type = 'exact'
	filtro = columna_query + '__' + search_type
	#info=members.filter(**{ filter: search_string })


	ingresos_rows = SysParameters.objects.filter(parameter="filas_ingresos").first()
	ingresos = Inoxoneuo.objects.select_related("empresa").filter(**{ filtro: 1}).filter(horaArribo__gte= h)[:int(ingresos_rows.value)]
	context = {'ingresos': ingresos, 'y': y, 'h':h}



	def get(self, request, *args, **kwargs):
		if request.is_ajax():
			lista_ingresos = []
			for ingreso in ingresos:
				data_ingreso = {}
				data_ingreso ['id'] = ingreso.id
				data_ingreso ['origen'] = ingreso.origen.denominacion
				data_ingreso['hora_partida'] = ingreso.horaPartida
				data_ingreso['hora_arribo'] = ingreso.horaArribo
				data_ingreso['empresa'] = ingreso.empresa.razonSocial
				lista_ingresos.append(data_ingreso)
			data = json.dumps(lista_ingresos)

			return HttpResponse(data,'application/json')

		else:
			return redirect ('terminal:vermonitor')


def ListaBoleteriaEmpresa(request, id_boleteria):
	boleteria = Nmen.objects.get(id=id_boleteria)
	asociadas = NmenIqueuoxon.objects.filter(boleteria=id_boleteria)
	empresas = Iqueuoxon.objects.all()
	context = {'boleteria':boleteria, 'asociadas':asociadas, 'empresas':empresas}

	return render (request, 'nachaxan/nmeniqueuoxon.html', context)

#2022 11 25 esto anda-----------------------------------------------------------
#class NuevaBoleteriaEmpresa(CreateView):
#	model = NmenIqueuoxon
#	form_class= NmenIqueuoxonForm
#	template_name = 'iot/nmeniqueuoxon.html'
#	success_url = reverse_lazy('terminal:lista_nmen_iqueuoxon')
#-------------------------------------------------------------------------------
#	def form_valid(self, form):
#		form.instance.boleteria_id = self.kwargs.get('pk')
#		return super(NuevaBoleteriaEmpresa, self).form_valid(form)

#def NuevaBoleteriaEmpresa (request, id_boleteria):
#	if request.method == 'POST':
#		nuevaboleteriaform = NmenIqueuoxonForm(request.POST)
#
#		if nuevaboleteriaform.is_valid():
#			nuevaboleteriaform.save()
#			return redirect('terminal:tablon')
#	else:
#		nuevaboleteriaform = NmenIqueuoxonForm()
#		boleteria = id_boleteria
#		context = {'nuevaboleteriaform' : nuevaboleteriaform, 'boleteria' : boleteria}
#	return render (request, 'iot/nmeniqueuoxon.html', context)

class VerBoleteria (DetailView):
	model = Nmen

	def get_context_data(self, *args, **kwargs):
		context = super(VerBoleteria, self).get_context_data(*args, **kwargs)

		#Acá añado al contexto las boleterías que tienen esta empresa asociada
		context['empresas'] = NmenIqueuoxon.objects.filter(boleteria=self.object)
		return context

	template_name = 'nachaxan/nmenDetails.html'



def NuevaBoleteriaEmpresa (request, id_boleteria):
	#genero el contexto
	#primero busco las empresas que aún no han sido asociadas a esta boleteria
	bol = id_boleteria
	boleteria = Nmen.objects.get(id=id_boleteria)

	#la siguiente línea me llevó mucho tiempo descubrir cómo se hacía
	#traigo los id de empresas que se encuentren con el id de boletería como filtro
	#de ese modo, recupero las empresas asociadas a esa boletería
	empresasAsociadas = NmenIqueuoxon.objects.values_list('empresa').filter(boleteria=bol)

	#y luego con un exclude obtengo las no asociadas todavía
	empresasNoAsociadas = Iqueuoxon.objects.exclude(id__in=empresasAsociadas)

	context = {'boleteria' : boleteria,
				'asociadas' : empresasAsociadas,
				'noAsociadas': empresasNoAsociadas}

	form = NmenIqueuoxonForm (request.POST or None)

	if request.method == 'POST':

		if form.is_valid():
			form.save()
		return redirect ('terminal:lista_nmen_iqueuoxon', id_boleteria = bol)

	context['form'] = form
	return render (request, "iot/nmeniqueuoxon.html", context)



class EliminarBoleteriaEmpresa(DeleteView):
	model = NmenIqueuoxon
	template_name = 'aqtac/nmeniqueuoxon.html'

	#debo enviar el id de la boletería que estoy viendo
	#me basé en lo publicado en:
	#https://stackoverflow.com/questions/26290415/deleteview-with-a-dynamic-success-url-dependent-on-id/26291235

	def get_success_url(self):
		boleteria = self.object.boleteria
		#mando como argumento el parámetro id_boleteria
		return reverse_lazy('terminal:lista_nmen_iqueuoxon', kwargs={'id_boleteria': boleteria.pk})
