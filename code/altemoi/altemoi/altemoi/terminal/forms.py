from django import forms

from terminal.models import SysParameters, Lvidaqa, Lasom, Lmala, Nmen, Status, Alhua, Alhuana, Yinoyic, Iqueuoxon, Inoxoneuo, InoxoneuoIntermedio,  Deyachi, DeyachiIntermedio, NmenIqueuoxon

class ParametroForm (forms.ModelForm):
    class Meta:
        model = SysParameters

        fields = [
            'parameter',
            'value',

        ]

        labels = {
            'parameter':'Parámetro',
            'value':'Valor',

         }

        widgets = {
             'parameter':forms.TextInput(attrs={'class':'form-control'}),
             'value':forms.TextInput(attrs={'class':'form-control'}),

         }

class LvidaqaForm (forms.ModelForm):
    class Meta:
        model = Lvidaqa

        fields = [
            'denominacion',
            'domicilio',
            'telefono',
            'email',
            'nro_impositivo',
        ]

        labels = {
            'denominacion':'Denominación',
            'domicilio' : 'Domicilio',
            'telefono':'Teléfono',
            'email':'Correo electrónico',
            'nro_impositivo':'Identificación tributaria',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'domicilio': forms.TextInput(attrs={'class':'form-control'}),
             'telefono' : forms.TextInput(attrs={'class':'form-control'}),
             'email' : forms.EmailInput(attrs={'class':'form-control'}),
             'nro_impositivo' : forms.TextInput(attrs={'class':'form-control'}),
         }

class LasomForm (forms.ModelForm):
    class Meta:
        model = Lasom

        fields = [
            'terminal',
            'denominacion',
        ]

        labels = {
            'terminal':'Estación terminal',
            'denominacion':'Denominación o código',
        }

        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'denominacion':forms.TextInput(attrs={'class':'form-control'}),

        }

class LmalaForm (forms.ModelForm):
    class Meta:
        model = Lmala

        fields = [
            'terminal',
            'puertaembarque',
            'numeroID',
            'denominacion',
            'rutaAudio',

        ]

        help_texts = {
        'denominacion':'Coloque el número de plataforma aquí',
        }

        labels = {
            'terminal':'Estación terminal',
            'puertaembarque':'Puerta de embarque',
            'numeroID':'Número ID',
            'denominacion':'Denominación',
            'rutaAudio':'Ruta de archivo de audio',

        }

        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'puertaembarque':forms.Select(attrs={'class':'form-control'}),
            'numeroID':forms.TextInput(attrs={'class':'form-control'}),
            'denominacion':forms.TextInput(attrs={'class':'form-control'}),
            'rutaAudio':forms.TextInput(attrs={'class':'form-control'}),

        }

class NmenForm (forms.ModelForm):
    class Meta:
        model = Nmen

        fields = [
            'terminal',
            'numeroID',
            'telefono',
            'internoTelefono',
            'email',
            'nombreContacto',
            'telefonoEmergencia',

            'status',

        ]

        labels = {
            'terminal':'Estación terminal',
            'numeroID':'Número ID',
            'telefono':'Teléfono público',
            'internoTelefono':'Número interno',
            'email':'Correo electrónico',
            'nombreContacto':'Apellido y nombre responsable',
            'telefonoEmergencia':'Contacto de responsable',

            'status':'Estado de la boletería',


        }
        widgets = {
            'terminal':forms.Select(attrs={'class':'form-control'}),
            'numeroID':forms.TextInput(attrs={'class':'form-control'}),
            'telefono':forms.TextInput(attrs={'class':'form-control'}),
            'internoTelefono':forms.TextInput(attrs={'class':'form-control'}),
            'email':forms.EmailInput(attrs={'class':'form-control'}),
            'nombreContacto':forms.TextInput(attrs={'class':'form-control'}),
            'telefonoEmergencia':forms.TextInput(attrs={'class':'form-control'}),
            'status':forms.Select(attrs={'class':'form-control'}),

        }

class StatusForm (forms.ModelForm):
    class Meta:
        model = Status

        fields = [
            'denominacion',
            'color',

        ]

        labels = {
            'denominacion':'Denominación',
            'color':'Color',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'color':forms.TextInput(attrs={'type':'color'}),

         }


class AlhuaForm (forms.ModelForm):
    class Meta:
        model = Alhua

        fields = [
            'denominacion',
            'code',
        ]

        labels = {
            'denominacion':'Denominación',
            'code' : 'Código',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'code': forms.TextInput(attrs={'class':'form-control'}),
         }

class AlhuanaForm (forms.ModelForm):
    class Meta:
        model = Alhuana

        fields = [
            'denominacion',
            'pais',
        ]

        labels = {
            'denominacion':'Denominación',
            'pais' : 'País',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'pais': forms.Select(attrs={'class':'form-control'}),
         }

class YinoyicForm (forms.ModelForm):
    class Meta:
        model = Yinoyic

        fields = [
            'denominacion',
            'provincia',
        ]

        labels = {
            'denominacion':'Denominación',
            'provincia' : 'Provincia',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'pais': forms.Select(attrs={'class':'form-control'}),
         }

class IqueuoxonFom (forms.ModelForm):
    class Meta:
        model = Iqueuoxon

        fields = [
            'terminal',
            'razonSocial',
            'nombreFantasia',
            'nro_impositivo',
            'domicilioFiscal',
            'telefono',
            'email',
            'sitioWeb',
        ]

        labels = {
            'terminal':'Terminal',
            'razonSocial' : 'Razón Social',
            'nombreFantasia':'Nombre comercial',
            'domicilioFiscal':'Domicilio fiscal',
            'telefono':'Teléfono',
            'email':'Correo electrónico',
            'nro_impositivo':'Identificación tributaria',
            'sitioWeb':'Sitio web',
         }

        widgets = {

             'terminal':forms.Select(attrs={'class':'form-control'}),
             'razonSocial':forms.TextInput(attrs={'class':'form-control'}),
             'nombreFantasia':forms.TextInput(attrs={'class':'form-control'}),
             'domicilioFiscal': forms.TextInput(attrs={'class':'form-control'}),
             'telefono' : forms.TextInput(attrs={'class':'form-control'}),
             'email' : forms.EmailInput(attrs={'class':'form-control'}),
             'nro_impositivo' : forms.TextInput(attrs={'class':'form-control'}),
             'sitioWeb' : forms.TextInput(attrs={'class':'form-control'}),

         }

class InoxoneuoForm (forms.ModelForm):
    class Meta:
        model = Inoxoneuo

        fields = [
            'terminal',
            'empresa',
            'origen',
            'intermedia',
            'horaPartida',
            'horaArribo',
            'd',
            'l',
            'm',
            'x',
            'j',
            'v',
            's',
            'puertaDefault',
        ]

        labels = {
            'terminal':'Terminal',
            'empresa':'Empresa',
            'origen':'Origen',
            'intermedia' : 'Continúa recorrido',
            'horaPartida':'Hora programada de partida (inicio) hacia terminal',
            'horaArribo':'Hora programada de arribo a terminal',
            'd':'Domingo',
            'l':'Lunes',
            'm':'Martes',
            'x':'Miércoles',
            'j':'Jueves',
            'v':'Viernes',
            's':'Sábado',
            'puertaDefault':'Puerta de desembarque predeterminada',
         }

        widgets = {

         'terminal':forms.Select(attrs={'class':'form-control'}),
         'empresa':forms.Select(attrs={'class':'form-control'}),
         'origen':forms.Select(attrs={'class':'form-control'}),
         'intermedia':forms.CheckboxInput(attrs={'class':'form-control'}),
         'horaPartida':forms.TimeInput(attrs={'class':'form-control', 'type':'time'}),
         'horaArribo':forms.TimeInput(attrs={'class':'form-control', 'type':'time'}),
         'd':forms.CheckboxInput(attrs={'class':'form-control'}),
         'l':forms.CheckboxInput(attrs={'class':'form-control'}),
         'm':forms.CheckboxInput(attrs={'class':'form-control'}),
         'x':forms.CheckboxInput(attrs={'class':'form-control'}),
         'v':forms.CheckboxInput(attrs={'class':'form-control'}),
         'j':forms.CheckboxInput(attrs={'class':'form-control'}),
         's':forms.CheckboxInput(attrs={'class':'form-control'}),
         'puertaDefault':forms.Select(attrs={'class':'form-control'}),
         }

class InoxoneuoIntermedioForm (forms.ModelForm):
    class Meta:
        model = InoxoneuoIntermedio

        fields = [
            'ciudad',
            'inoxoneuo',
        ]

class DeyachiIntermedioForm (forms.ModelForm):
    class Meta:
        model = DeyachiIntermedio

        fields = [
            'ciudad',
            'deyachi',
        ]

class DeyachiForm (forms.ModelForm):
    class Meta:
        model = Deyachi

        fields = [
            'terminal',
            'empresa',
            'destino',
            'intermedia',
            'horaPartida',
            'd',
            'l',
            'm',
            'x',
            'j',
            'v',
            's',
            'puertaDefault',
        ]

        labels = {
            'terminal':'Terminal',
            'empresa':'Empresa',
            'destino':'Destino',
            'intermedia' : 'Destinos intermedios',
            'horaPartida':'Hora programada de partida de la terminal',
            'd':'Domingo',
            'l':'Lunes',
            'm':'Martes',
            'x':'Miércoles',
            'j':'Jueves',
            'v':'Viernes',
            's':'Sábado',
            'puertaDefault':'Puerta de embarque predeterminada',
         }

        widgets = {

             'terminal':forms.Select(attrs={'class':'form-control'}),
             'empresa':forms.Select(attrs={'class':'form-control'}),
             'destino':forms.Select(attrs={'class':'form-control'}),
             'intermedia':forms.CheckboxInput(attrs={'class':'form-control'}),
             'horaPartida':forms.TimeInput(attrs={'class':'form-control', 'type':'time'}),
             'd':forms.CheckboxInput(attrs={'class':'form-control'}),
             'l':forms.CheckboxInput(attrs={'class':'form-control'}),
             'm':forms.CheckboxInput(attrs={'class':'form-control'}),
             'x':forms.CheckboxInput(attrs={'class':'form-control'}),
             'v':forms.CheckboxInput(attrs={'class':'form-control'}),
             'j':forms.CheckboxInput(attrs={'class':'form-control'}),
             's':forms.CheckboxInput(attrs={'class':'form-control'}),
             'puertaDefault':forms.Select(attrs={'class':'form-control'}),


         }
class NmenIqueuoxonForm (forms.ModelForm):
    class Meta:
        model = NmenIqueuoxon

        fields = [
            'empresa',
            'boleteria',


        ]
