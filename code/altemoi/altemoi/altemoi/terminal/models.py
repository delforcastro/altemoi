from django.db import models
from django.contrib.auth.models import User

#Los modelos de la aplicación Terminal están pensados
#para la administración de la estructura  física propia de la Terminal
#incluye datos generales de la estación terminal, plataformas, boleterías

class SysParameters(models.Model):
    id = models.BigAutoField(primary_key=True)
    parameter = models.CharField(max_length=150)
    value = models.CharField(max_length=1509)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)


class Status (models.Model):
    id = models.BigAutoField(primary_key=True)
    denominacion = models.CharField(max_length=150)
    color = models.CharField(max_length=7, default='#FFFFFF')
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion

class Alhua (models.Model):
    #'alhua signifia 'tierra, país'
    #'alhua means 'Land'
    #Esta clase representa los países
    id = models.BigAutoField(primary_key=True)

    denominacion = models.CharField(max_length=150)
    code = models.CharField(max_length=10)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion


class Alhuana (models.Model):
    #'alhuana significa 'tierra, zona'
    #'alhuana means 'land'
    #Esta clase representa las provincias
    id = models.BigAutoField(primary_key=True)

    denominacion = models.CharField(max_length=150)

    #FK para asociar a País
    pais = models.ForeignKey(Alhua, on_delete=models.SET_NULL, null=True, blank=True)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion + "(" + self.pais.code + ")"

class Yinoyic (models.Model):
    #yi noyic significa 'el pueblo'
    #yi noyic means 'town, village'
    #Esta clase representa las ciudades
    id = models.BigAutoField(primary_key=True)

    denominacion = models.CharField(max_length=150)

    #FK para asociar a Provincia
    provincia = models.ForeignKey(Alhuana, on_delete=models.SET_NULL, null=True, blank=True)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion + " (" + self.provincia.denominacion +  "-" + self.provincia.pais.code +")"


class Lvidaqa (models.Model):
    #'Lvidaqa' significa 'Terminal'
    #'Lvidaqa' means 'Station'
    #Esta clase representa la Terminal
    id = models.BigAutoField(primary_key=True)
    
    
    denominacion = models.CharField(max_length=150)
    domicilio = models.CharField(max_length=150, null=True, blank=True)
    telefono = models.CharField(max_length=40, blank=True)
    email = models.EmailField(max_length=150)
    #nro_impositivo está pensado para almacenar un número
    #de identificación tributaria, como CUIT en Argentina.
    nro_impositivo = models.CharField(max_length=20, blank=True)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    def __str__(self):
        return self.denominacion

class Lasom (models.Model):
    #Lasom significa 'Puerta'
    #Lasom means 'Door'
    #Esta clase representa las Puertas de embarque

    id = models.BigAutoField(primary_key=True)

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la puerta de embarque:
    denominacion = models.CharField(max_length=150)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion

class Lmala (models.Model):
    #Lmala' significa 'Plataforma'
    #Lmala' means 'Platform'
    #Esta clase representa las plataformas físicas de la terminal
    id = models.BigAutoField(primary_key=True)

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la plataforma:
    numeroID = models.IntegerField()
    denominacion = models.CharField(max_length=30)
    rutaAudio = models.CharField(max_length=150, null=True, blank=True)
    puertaembarque = models.ForeignKey(Lasom, on_delete=models.CASCADE)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)


    def __str__(self):
        return self.denominacion


class Iqueuoxon (models.Model):
    #Iqueuoxon significa 'lo lleva de viaje'
    #Iqueuoxon means ''
    #Esta clase representa a las empresas de transporte
    id = models.BigAutoField(primary_key=True)

    #FK para asociar a terminal
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)

    #Datos de la empresa de transporte
    razonSocial = models.CharField(max_length=150, null=False, blank=False)
    nombreFantasia = models.CharField(max_length=150, null=False, blank=False)

    #nro_impositivo está pensado para almacenar un número
    #de identificación tributaria, como CUIT en Argentina.
    nro_impositivo = models.CharField(max_length=20, blank=True)
    domicilioFiscal = models.CharField(max_length=150, blank=True)
    telefono = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    sitioWeb = models.CharField(max_length=100, null=True, blank=True)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.razonSocial

class Nmen (models.Model):
    #Nmen significa '[él] Vende'
    #Nmen means '[he] Sells'
    #Esta clase representa las Boleterías de la terminal
    id = models.BigAutoField(primary_key=True)


    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)
    #Datos de la boletería
    numeroID = models.IntegerField()
    telefono = models.CharField(max_length=50, null=True, blank=True)
    internoTelefono = models.CharField(max_length=10, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    nombreContacto = models.CharField(max_length=150)
    telefonoEmergencia = models.CharField(max_length=50, null=True, blank=True)
    flag = models.IntegerField(null=True, blank=True)

    #En esta relación muchos a muchos establecemos la asociación entre
    #boleterías y empresas de transporte (muchos a muchos)


    #Estado podría ser: 1 - Activa, normal, 2: Activa, con observacion
    #3: Inactiva, temporalmente, 4: Inactiva indeterminadamente

    status = models.ForeignKey(Status, on_delete=models.CASCADE)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return "Boletería" + str(self.numeroID)

class NmenIqueuoxon (models.Model):
    #Permite asociar qué empresas se atienden en cuáles boleterías
    id = models.BigAutoField(primary_key=True)
    
    boleteria = models.ForeignKey(Nmen, on_delete=models.CASCADE)
    empresa = models.ForeignKey(Iqueuoxon, on_delete=models.SET_NULL, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

class Inoxoneuo (models.Model):
    #Inoxoneuo significa 'entrar [con intención de llegar] hacia'
    #Inoxoneuo means 'arrive [with intention of keep moving on] to'
    #Esta clase representa los arribos de servicios
    id = models.BigAutoField(primary_key=True)

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)

    #Datos de servicios
    origen = models.ForeignKey(Yinoyic, on_delete=models.SET_NULL, null=True)
    intermedia = models.BooleanField(default=False)
    horaPartida = models.TimeField(null=True, blank=True)
    horaArribo = models.TimeField(null=False, blank=False)
    empresa = models.ForeignKey(Iqueuoxon, on_delete=models.SET_NULL, null=True)
    d = models.BooleanField(default=False)
    l = models.BooleanField(default=False)
    m = models.BooleanField(default=False)
    x = models.BooleanField(default=False)
    j = models.BooleanField(default=False)
    v = models.BooleanField(default=False)
    s = models.BooleanField(default=False)
    puertaDefault = models.ForeignKey(Lasom, on_delete=models.SET_NULL, null=True)




class Deyachi (models.Model):
    #Deyachi significa 'que sale [mucho, o con ímpetu]'
    #Deyachi means 'come out [strongly]'
    #Esta clase representa las partidas de servicios
    id = models.BigAutoField(primary_key=True)

    #FK para asociar a terminal:
    terminal = models.ForeignKey(Lvidaqa, on_delete=models.CASCADE)

    #Datos de servicios
    destino = models.ForeignKey(Yinoyic, on_delete=models.SET_NULL, null=True)
    intermedia = models.BooleanField(default=False)
    horaPartida = models.TimeField(null=False, blank=False)
    horaArribo = models.TimeField(null=True, blank=True)
    empresa = models.ForeignKey(Iqueuoxon, on_delete=models.SET_NULL, null=True)
    d = models.BooleanField(default=False)
    l = models.BooleanField(default=False)
    m = models.BooleanField(default=False)
    x = models.BooleanField(default=False)
    j = models.BooleanField(default=False)
    v = models.BooleanField(default=False)
    s = models.BooleanField(default=False)
    puertaDefault = models.ForeignKey(Lasom, on_delete=models.SET_NULL, null=True)

class DeyachiIntermedio (models.Model):
    id = models.BigAutoField(primary_key=True)

    deyachi = models.ForeignKey(Deyachi, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Yinoyic, on_delete=models.SET_NULL, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

class InoxoneuoIntermedio (models.Model):
    #ciudades intermedias por las que pasa un servicio ingresante
    id = models.BigAutoField(primary_key=True)

    inoxoneuo = models.ForeignKey(Inoxoneuo, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(Yinoyic, on_delete=models.SET_NULL, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
