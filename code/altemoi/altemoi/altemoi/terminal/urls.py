from django.urls import include, path, re_path
from django.contrib.auth.decorators import login_required

from . import views
app_name = "terminal"

urlpatterns = [
                path('tablon', login_required(views.Tablon), name='tablon'),

                #vista de monitor de Entradas y Salidas
                path('monitor', login_required(views.Monitor), name='monitor'),
                path('vermonitor', login_required(views.VerMonitor), name='vermonitor'),
                path('nuevaterminal', login_required(views.NuevaTerminal.as_view()), name='nueva_lvidaqa'),
                path('listaterminal', login_required(views.ListaTerminal.as_view()), name='lista_lvidaqa'),
                re_path(r'^editarterminal/(?P<pk>\d+)/$', login_required(views.EditarTerminal.as_view()), name='editar_lvidaqa'),
                re_path(r'^eliminarterminal/(?P<pk>\d+)/$', login_required(views.EliminarTerminal.as_view()), name='eliminar_lvidaqa'),


                path('nuevapuerta', login_required(views.NuevaPuerta.as_view()), name='nueva_lasom'),
                path('listapuerta', login_required(views.ListaPuerta.as_view()), name='lista_lasom'),
                re_path(r'^editarpuerta/(?P<pk>\d+)/$', login_required(views.EditarPuerta.as_view()), name='editar_lasom'),
                re_path(r'^eliminarpuerta/(?P<pk>\d+)/$', login_required(views.EliminarPuerta.as_view()), name='eliminar_lasom'),


                path('nuevaplataforma', login_required(views.NuevaPlataforma.as_view()), name='nueva_lmala'),
                path('listaplataforma', login_required(views.ListaPlataforma.as_view()), name='lista_lmala'),
                re_path(r'^editarplataforma/(?P<pk>\d+)/$', login_required(views.EditarPlataforma.as_view()), name='editar_lmala'),
                re_path(r'^eliminarplataforma/(?P<pk>\d+)/$', login_required(views.EliminarPlataforma.as_view()), name='eliminar_lmala'),


                path('nuevaboleteria', login_required(views.NuevaBoleteria.as_view()), name='nueva_nmen'),
                path('listaboleteria', login_required(views.ListaBoleteria.as_view()), name='lista_nmen'),
                re_path(r'^editarboleteria/(?P<pk>\d+)/$', login_required(views.EditarBoleteria.as_view()), name='editar_nmen'),
                re_path(r'^eliminarboleteria/(?P<pk>\d+)/$', login_required(views.ElimininarBoleteria.as_view()), name='eliminar_nmen'), #falta implementar esto


                path('nuevostatus', login_required(views.NuevoStatus.as_view()), name='nuevo_status'),
                path('listastatus', login_required(views.ListaStatus.as_view()), name='lista_status'),
                re_path(r'^editarstatus/(?P<pk>\d+)/$', login_required(views.EditarStatus.as_view()), name='editar_status'),
                re_path(r'^eliminarstatus/(?P<pk>\d+)/$', login_required(views.EliminarStatus.as_view()), name='eliminar_status'),

                #paths para configuración de países, provincias, ciudades

                #países
                path('nuevopais', login_required(views.NuevoPais.as_view()), name='nuevo_alhua'),
                path('listapais', login_required(views.ListaPais.as_view()), name='lista_alhua'),
                re_path(r'^editarpais/(?P<pk>\d+)/$', login_required(views.EditarPais.as_view()), name='editar_alhua'),
                re_path(r'^eliminarpais/(?P<pk>\d+)/$', login_required(views.EliminarPais.as_view()), name='eliminar_alhua'),

                #provincias
                path('nuevaprovincia', login_required(views.NuevaProvincia.as_view()), name='nueva_alhuana'),
                path('listaprovincia', login_required(views.ListaProvincia.as_view()), name='lista_alhuana'),
                re_path(r'^editarprovincia/(?P<pk>\d+)/$', login_required(views.EditarProvincia.as_view()), name='editar_alhuana'),
                re_path(r'^eliminarprovincia/(?P<pk>\d+)/$', login_required(views.EliminarProvincia.as_view()), name='eliminar_alhuana'),

                #ciudades
                path('nuevaciudad', login_required(views.NuevaCiudad.as_view()), name='nueva_yinoyic'),
                path('listaciudad', login_required(views.ListaCiudad.as_view()), name='lista_yinoyic'),
                re_path(r'^editarciudad/(?P<pk>\d+)/$', login_required(views.EditarCiudad.as_view()), name='editar_yinoyic'),
                re_path(r'^eliminarciudad/(?P<pk>\d+)/$', login_required(views.EliminarCiudad.as_view()), name='eliminar_yinoyic'),


                #empresas de transporte
                path('nuevaempresa', login_required(views.NuevaEmpresa.as_view()), name='nueva_iqueuoxon'),
                path('listaempresa', login_required(views.ListaEmpresa.as_view()), name='lista_iqueuoxon'),
                re_path(r'^verempresa/(?P<pk>\d+)/$', login_required(views.VerEmpresa.as_view()), name='ver_iqueuoxon'),
                re_path(r'^editarempresa/(?P<pk>\d+)/$', login_required(views.EditarEmpresa.as_view()), name='editar_iqueuoxon'),
                re_path(r'^eliminarempresa/(?P<pk>\d+)/$', login_required(views.EliminarEmpresa.as_view()), name='eliminar_iqueuoxon'),

                #servicios de transporte ingresantes
                path('nuevoingreso', login_required(views.NuevoIngreso.as_view()), name='nuevo_inoxoneuo'),
                path('listaingreso', login_required(views.ListaIngreso.as_view()), name='lista_inoxoneuo'),
                re_path(r'^editaringreso/(?P<pk>\d+)/$', login_required(views.EditarIngreso.as_view()), name='editar_inoxoneuo'),
                re_path(r'^eliminaringreso/(?P<pk>\d+)/$', login_required(views.EliminarIngreso.as_view()), name='eliminar_inoxoneuo'),

                #Detalle del servicio ingresante
                #la forma de llamarla es levemente diferente porque es una vista basada en función
                path('detalleingreso/<int:id_ingreso>/', login_required(views.DetalleIngreso), name='detalle_inoxoneuo'),
                path('nuevoingresointermedio/<int:id_ingreso>/', login_required(views.NuevoIngresoIntermedio), name='nuevo_inoxoneuo_intermedio'),
                re_path(r'^eliminaringresointermedio/(?P<pk>\d+)/$', login_required(views.EliminarIngresoIntermedio.as_view()), name='eliminar_inoxoneuo_intermedio'),

                #Detalle del servicio saliente
                #la forma de llamarla es levemente diferente porque es una vista basada en función
                path('detalleegreso/<int:id_egreso>/', login_required(views.DetalleEgreso), name='detalle_deyachi'),
                path('nuevoegresointermedio/<int:id_egreso>/', login_required(views.NuevoEgresoIntermedio), name='nuevo_deyachi_intermedio'),
                re_path(r'^eliminaregresointermedio/(?P<pk>\d+)/$', login_required(views.EliminarEgresoIntermedio.as_view()), name='eliminar_deyachi_intermedio'),

                #paths para asociar empresas a boleterías
                #path('nuevaboleteriaempresa', login_required(views.NuevaBoleteriaEmpresa.as_view()), name='nuevo_nmen_iqueuoxon'),
                path('nuevaboleteriaempresa/<int:id_boleteria>/', login_required(views.NuevaBoleteriaEmpresa), name='nuevo_nmen_iqueuoxon'),
                re_path(r'^verboleteria/(?P<pk>\d+)/$', login_required(views.VerBoleteria.as_view()), name='ver_nmen'),
                path('listaboleteriaempresa/<int:id_boleteria>/', login_required(views.ListaBoleteriaEmpresa), name='lista_nmen_iqueuoxon'),
                re_path(r'^eliminarboleteriaempresa/(?P<pk>\d+)/$', login_required(views.EliminarBoleteriaEmpresa.as_view()), name='eliminar_nmen_iqueuoxon'),


                #servicios de transporte salientes
                path('nuevoegreso', login_required(views.NuevoEgreso.as_view()), name='nuevo_deyachi'),
                path('listaegreso', login_required(views.ListaEgreso.as_view()), name='lista_deyachi'),
                #path('listaegreso', login_required(views.ListaEgreso), name='lista_deyachi'),
                re_path(r'^editaregreso/(?P<pk>\d+)/$', login_required(views.EditarEgreso.as_view()), name='editar_deyachi'),
                re_path(r'^eliminaregreso/(?P<pk>\d+)/$', login_required(views.EliminarEgreso.as_view()), name='eliminar_deyachi'),



                #parámetros del Sistema
                path('nuevoparametro', login_required(views.NuevoParametro.as_view()), name='nuevo_parametro'),
                path('listaparametro', login_required(views.ListaParametro.as_view()), name='lista_parametro'),
                re_path(r'^editarparametro/(?P<pk>\d+)/$', login_required(views.EditarParametro.as_view()), name='editar_parametro'),
                re_path(r'^eliminarparametro/(?P<pk>\d+)/$', login_required(views.EliminarParametro.as_view()), name='eliminar_parametro'),

]
